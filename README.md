# Student Performance Analysis



## About 

On the Student Performance Analysis website, we believe that everyone deserves to see and appreciate the result that Bhutanese students of 12 th standard have acquired. Our mission has remained the same since day one. The website will contribute to strengthening the education strategies in the science stream by predicting future results based on past results. Our Website provides five year analysis of class 12 science result from 2012 till 2019. We show different analysis in the form of graphs. We have used bar graphs,pi-chart and etc. We have created an iteractive dashboard whereby its eaiser for users to navigate and understand the graphs we show. We have a machine learning implemented model thats lets us see if we got PCA or PCNA by entering our marks. We have used Decision Tree classifier algorithm to study and learn from the 7 years data of class 12 science result.

## Technology Used
1. Software Requirements
    - Python
    - Operating system (windows or Ubuntu) 
    - Google colab 
    - Jupyter Notebook 
    - Django
    - GitLab
    - Heruko
    - Flask
2. Hardware Requirements
    - Laptop with i5 and above
    - 8GB RAM recommended

## Links for our Website and promotional video

Visit our Website on = https://student-perfromance-analysis.herokuapp.com/

Link for promotional Video = https://www.youtube.com/watch?v=jLeh4RhAsAo

## Poster for our project	

<img src= "Pictures/poster.jpeg" width="200" height = "290">


## Screenshots of Web Application

<img src= "Pictures/home.jpeg" >
<img src= "Pictures/home2.jpeg" >
<img src= "Pictures/home3.jpeg" >
<img src= "Pictures/home4.jpeg" >
<img src= "Pictures/home5.jpeg" >
<img src= "Pictures/1.jpeg" >

<img src= "Pictures/2.jpeg" >

<img src= "Pictures/3.jpeg" >

<img src= "Pictures/4.jpeg" >

<img src= "Pictures/prediction.jpeg" >









































